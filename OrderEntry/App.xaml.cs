﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using OrderEntry.Views;
using OrderEntry.Models;
using OrderEntry.Services;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace OrderEntry
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            AutomapperConfig.RegisterMappings();

            MainPage = new LoginPage();

            CurrentSettings = new Settings();
        }

        public static Settings CurrentSettings { get; set; }

        protected override void OnStart()
        {

            //load settings
            var service = new SettingsService();
            CurrentSettings = service.SettingsFromProperties();

            //override with Dev settings
            CurrentSettings.ServicesVersion = "2.0";
            CurrentSettings.Mode = Mode.Dev;
            CurrentSettings.AssedId = "IPAD2304&";
            CurrentSettings.BaseURL = "http://r7development1.eecol.lan/run/operations/Start?mobile=1";


        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps

            //save settings
            var service = new SettingsService();
            service.AddSettingsToProperties(CurrentSettings);

        }

        protected override void OnResume()
        {
            // Handle when your app resumes


        }
    }
}
