﻿using System;
using Newtonsoft.Json;
using Xamarin.Forms;
using OrderEntry.Models;

namespace OrderEntry.Services
{
    public class SettingsService
    {
        public SettingsService()
        {
        }

        public void AddSettingsToProperties(Settings settings){
            
            var settingsJson = JsonConvert.SerializeObject(settings);
            if (Application.Current.Properties.ContainsKey("Settings"))
            {
                Application.Current.Properties["Settings"] = settingsJson;
            }
            else
            {
                Application.Current.Properties.Add("Settings", settingsJson);
            }
            Application.Current.SavePropertiesAsync();

        }

        public  Settings SettingsFromProperties(){
            Settings setting = new Settings();
           
            if (Application.Current.Properties.ContainsKey("Settings"))
            {
                setting = JsonConvert.DeserializeObject<Settings>(Application.Current.Properties["Settings"].ToString());
            }
    
            return setting;

        }
    }
}
