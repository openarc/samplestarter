﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using OrderEntry.Models;

namespace OrderEntry.Services
{
    public class BaseService<T>
    {
        public BaseService()
        {

        }

        public SingleResultModel<T> ServiceResult { get; set; }

        //example post service
        public static async Task<T> PostService(Dictionary<string, string> formParameters, string job, string subjob)
        {
            DialogService dialog = new DialogService();

            FormUrlEncodedContent content = new FormUrlEncodedContent(formParameters);

            SingleResultModel<T> result = new SingleResultModel<T>();

            HttpClient serviceclient = new HttpClient();
           
            serviceclient.MaxResponseContentBufferSize = 256000;
           
            HttpResponseMessage response;
           
            //build base
            string serviceuri = App.CurrentSettings.BaseURL + "&session=" + App.CurrentSettings.CurrentSession + "&version=" + App.CurrentSettings.ServicesVersion + "&devicename=" + App.CurrentSettings.AssedId + "&job=" + job + "&subjob=" + subjob;

            Uri baseUri = new Uri(string.Format(serviceuri, string.Empty));
       


            try
            {

                response = await serviceclient.PostAsync(baseUri, content);
          
                if (response.IsSuccessStatusCode)
                {
                    var returnContent = await response.Content.ReadAsStringAsync();

                    result = JsonConvert.DeserializeObject<SingleResultModel<T>>(returnContent);
                    if (result.Result.Equals("Error", StringComparison.OrdinalIgnoreCase))
                    {
                        await dialog.ShowMessage(("There is a problem: " + result.Message), "Error");
                    }
                    else
                    {
                        Console.WriteLine("Service success {0} ", result.Message);
                    }
                    
                }
                else
                {
                    result.Message = response.ToString();

                    result.Result = "Error";
                    
                    Console.WriteLine("Service Error {0} , {1}",result.Message, result.Result );

                    await dialog.ShowMessage(("Service Error: " + result.Message + ", " + result.Result), "Error");
                }



            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: '{0}'", e);

                result.Result = "error";

                result.Message = e.InnerException.ToString();

                await dialog.ShowError(e, "Service Exception", "OK", null);
            }

            return result.Data;

        }



    }
}
