﻿using System;
using AutoMapper;
using OrderEntry.Models;

namespace OrderEntry.Services
{
    public static class AutomapperConfig
    {

        public static void RegisterMappings()
        {

            Mapper.Initialize(cfg =>
            {
               /*example
                 cfg.CreateMap<PickingSlipLineDTO, PickingSlipItemModel>()
                .ForMember(d => d.NumberAndDescription, o => o.MapFrom(s => (s.productnumber + ": " + s.description)))
                .IgnoreAllPropertiesWithAnInaccessibleSetter();
                */               

            }

          );
        }
     }
}