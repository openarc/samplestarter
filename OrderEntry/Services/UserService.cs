﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using OrderEntry.Models;

namespace OrderEntry.Services
{
    public class UserService
    {
        public UserService()
        {
        }

        public async Task<LoginModel> LoginWithBase(string username, string password){

          // BaseService service = new BaseService();

            var userInfo = new Dictionary<string, string>
            {
                {"USERNAME", username},
                { "PASSWORD", password}
             };

            FormUrlEncodedContent content = new FormUrlEncodedContent(userInfo);

            LoginModel result = new LoginModel();

            HttpClient serviceclient = new HttpClient();

            serviceclient.MaxResponseContentBufferSize = 256000;

            HttpResponseMessage response;

            //build base
            string serviceuri = App.CurrentSettings.BaseURL + "&version=" + App.CurrentSettings.ServicesVersion + "&devicename=" + App.CurrentSettings.AssedId + "&job=MobileLogin";

            Uri baseUri = new Uri(string.Format(serviceuri, string.Empty));



            try
            {

                response = await serviceclient.PostAsync(baseUri, content);

                if (response.IsSuccessStatusCode)
                {
                    var returnContent = await response.Content.ReadAsStringAsync();

                    result = JsonConvert.DeserializeObject<LoginModel>(returnContent);

                    Console.WriteLine("Service success {0} ", result.Message);
                }
                else
                {
                    result.Message = response.ToString();

                    result.Result = "Error";

                    Console.WriteLine("Service Error {0} , {1}", result.Message, result.Result);
                }



            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: '{0}'", e);
                result.Result = "error";
                result.Message = e.ToString();
            }

            return result;
        }


    }
}
