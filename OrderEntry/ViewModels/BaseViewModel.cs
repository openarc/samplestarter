﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Xamarin.Forms;
using System.Threading.Tasks;
using System.Reflection;
using System.Globalization;
using OrderEntry.Services;
using OrderEntry.Views;

namespace OrderEntry.ViewModels
{
    public class BaseViewModel : INotifyPropertyChanged
    {
        //implement as needed
        //public IRepository<U> Repository => DependencyService.Get<IRepository<U>>();

        bool isBusy = false;

        public bool IsBusy
        {
            get { return isBusy; }
            set { SetProperty(ref isBusy, value); }
        }

        string title = string.Empty;
        public string Title
        {
            get { return title; }
            set { SetProperty(ref title, value); }
        }

        protected bool SetProperty<T>(ref T backingStore, T value,
            [CallerMemberName]string propertyName = "",
            Action onChanged = null)
        {
            if (EqualityComparer<T>.Default.Equals(backingStore, value))
                return false;

            backingStore = value;
            onChanged?.Invoke();
            OnPropertyChanged(propertyName);
            return true;
        }

        public Task NavigateToAsync(BaseViewModel viewModel) 
        {
            return InternalNavigateToAsync(viewModel, null);
        }

        public Task NavigateToAsync(BaseViewModel viewModel, object parameter) 
        {
            return InternalNavigateToAsync(viewModel, parameter);
        }

        public Task PopPageAsync()
        {
            return InternalPopPageAsync();
        }

        public static DialogService Dialogs = new DialogService();

        public virtual async Task InitializeAsync(object navigationData)
        {

        }

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            var changed = PropertyChanged;
            if (changed == null)
                return;

            changed.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        #region navigation

        private async Task InternalNavigateToAsync(BaseViewModel viewModel, object parameter)
        {
            //NavigationPage page = CreatePage(viewModel, parameter);
            Page page = CreatePage(viewModel, parameter);
            if (page is LoginPage)
            {
                Application.Current.MainPage = new MainPage();
            }
            else
            {
              //update for tabbed interface
                TabbedPage main = (Xamarin.Forms.TabbedPage)Application.Current.MainPage;
                var navigationPage = main.CurrentPage as NavigationPage;

                await navigationPage.PushAsync(page);
       
            }

            await (page.BindingContext as BaseViewModel).InitializeAsync(parameter);
        }

        private Type GetPageTypeForViewModel(BaseViewModel viewModel)
        {
            var viewModelType = viewModel.GetType();
            var viewName = viewModelType.Name.Replace("ViewModel", "Page");
            var viewFullName = viewModelType.FullName.Replace(viewModelType.Name, viewName).Replace("ViewModel","View");
            var viewModelAssemblyName = viewModelType.GetTypeInfo().Assembly.FullName;
            var viewAssemblyName = string.Format(
                        CultureInfo.InvariantCulture, "{0}, {1}", viewFullName, viewModelAssemblyName);
            var viewType = Type.GetType(viewAssemblyName);
            return viewType;
        }

        private Page CreatePage(BaseViewModel viewModel, object parameter)
        {
            Type pageType = GetPageTypeForViewModel(viewModel);
            if (pageType == null)
            {
                throw new Exception($"Cannot locate page type for {viewModel.ToString()}");
            }

            object[] args = new object[1];
            args[0] = viewModel;
            Page page = Activator.CreateInstance(pageType,args) as Page;
           // var navPage = new NavigationPage(page);
            return page;
        }

        private async Task InternalPopPageAsync()
        {
            TabbedPage main = (Xamarin.Forms.TabbedPage)Application.Current.MainPage;
            var navigationPage = main.CurrentPage as NavigationPage;
            await navigationPage.PopAsync();
        }


        #endregion
    }
}
