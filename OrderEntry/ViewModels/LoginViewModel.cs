﻿using System;
using System.Threading.Tasks;
using OrderEntry.Models;
using OrderEntry.Services;
using OrderEntry.Views;
using Xamarin.Forms;

namespace OrderEntry.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {
        public LoginModel Item { get; set; }
        public Command LoginClickedCommand { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public bool ShowRequestAccess { get; set; }


        public LoginViewModel(LoginModel item = null)
        {
            Title = "Login";
            Item = item;
            LoginClickedCommand = new Command(async () => await ExecuteLoginClickedCommand());
            ShowRequestAccess = false;
           
        }


        private async Task ExecuteLoginClickedCommand()
        {
            //test login
            IsBusy = true;
            LoginModel result;
            UserService service = new UserService();

            try
            {


                if (App.CurrentSettings.Mode == Mode.Dev)
                {
                    result = await service.LoginWithBase("testapi", "gust842r");
                }
                else
                {
                    result = await service.LoginWithBase(Username, Password);
                }

                if ((!string.IsNullOrEmpty(result.Message) && result.Result.Equals("error")))
                {

                    await Dialogs.ShowMessage(result.Message, "Error Logging In");

                }
                else if (!string.IsNullOrEmpty(result.Session) && result.Result.Equals("success"))
                {

                    App.CurrentSettings.CurrentSession = result.Session;
                    App.CurrentSettings.CurrentUsername = result.Username;
                    Application.Current.MainPage = new MainPage();

                }
                else if (!string.IsNullOrEmpty(result.Message))
                {

                    await Dialogs.ShowMessage(result.Message, "Login Result");
                }
                else
                {

                    await Dialogs.ShowMessage("No Data", "Error Logging In");
                }


            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message.ToString());

                await Dialogs.ShowError(e, "Application Error", "OK", null);
            }
            finally
            {
                IsBusy = false;
            }

        }

        private void OpenMainClicked()
        {

            Application.Current.MainPage = new MainPage();

        }

        private void RequestAccessClicked()
        {

            ShowRequestAccess = !ShowRequestAccess;

        }

    }
}
