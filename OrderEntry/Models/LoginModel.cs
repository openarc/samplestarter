﻿using System;
namespace OrderEntry.Models
{
    public class LoginModel: BaseBindableModel
    {
        public string Username { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Session { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }

    }
}
