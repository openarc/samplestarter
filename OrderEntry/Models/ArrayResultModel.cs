﻿using System;
namespace OrderEntry.Models
{
    public class ArrayResultModel<T>
    {

        public String Message { get; set; }
        public String Result { get; set; }
        public T[] Data { get; set; }

        
    }
}
