﻿using System;
namespace OrderEntry.Models
{
    public class Settings
    {
        public String CurrentUserId { get; set; }
        public String CurrentUsername { get; set; }
        public Mode Mode { get; set; }
        public String CurrentSession { get; set; }
        public String ServicesVersion { get; set; }
        public String BaseURL { get; set; }
        public String AssedId { get; set; }

    }

    public enum Mode
    {
        Dev = 0, QA = 1, Prod = 2
    }

}
