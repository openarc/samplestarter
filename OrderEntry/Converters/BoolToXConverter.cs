﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace OrderEntry
{
    public class BoolToXConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is bool)) return null;
            bool index = (bool)value;

            if (index)
                return "X";
            else
                return "";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}