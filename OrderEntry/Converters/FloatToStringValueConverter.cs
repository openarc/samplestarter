﻿using System;
using Xamarin.Forms;

namespace OrderEntry
{
	public class FloatToStringValueConverter : IValueConverter
	{
		public object Convert (object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
	    {
	        float theFloat = (float)value;
	        return theFloat.ToString ();
	    }
 
		public object ConvertBack (object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			string strValue = value as string;
			if (string.IsNullOrEmpty (strValue))
			{
				return 0;
			}
	        float resultFloat;
	        if (float.TryParse (strValue, out resultFloat)) 
	        {
	            return resultFloat;
	        }
	        return 0;
	    }
	}
}


