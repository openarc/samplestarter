﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace OrderEntry
{
    public class BackgroundConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is bool)) return null;
            bool index = (bool)value;

            if (index)
                return Color.White;
            else
                return Color.FromHex("#c8c8ef");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}