﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace OrderEntry
{
    public class ListItemZebraConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is int)) return null;
            int index = (int)value;

            if ((index % 2) == 1)
            {
                return Color.White;
            }
            else
            {
                return Color.FromHex("#c8c8ef");
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
