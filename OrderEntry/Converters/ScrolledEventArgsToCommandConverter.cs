﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace OrderEntry
{
	public class ScrolledEventArgsToCommandConverter : IValueConverter
	{

		public object Convert (object value, Type targetType, object parameter, CultureInfo culture)
		{
			var eventArgs = value as ScrolledEventArgs;
			return eventArgs.ScrollY;
		}

		public object ConvertBack (object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException ();
		}
	}
}