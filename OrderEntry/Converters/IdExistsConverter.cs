﻿using System;
using Xamarin.Forms;
namespace OrderEntry
{
	public class IdExistsConverter :IValueConverter
	{
		public object Convert (object value, Type targetType, object parameter,
			System.Globalization.CultureInfo culture)
		{

			if (value == null || (string)value == String.Empty)
				return false;
					
			if (targetType != typeof (string)) {
				throw new InvalidOperationException ("The target must be a string");
			}
			string toCheck = value as string;
			if (toCheck.Length > 0)
				return true;
			else
				return false;
			
		}

		public object ConvertBack (object value, Type targetType, object parameter,
			System.Globalization.CultureInfo culture)
		{
			throw new NotSupportedException ();
		}
	}
}
