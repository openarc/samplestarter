﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using OrderEntry.Models;
using OrderEntry.ViewModels;
using OrderEntry.Services;
using System.Threading.Tasks;

namespace OrderEntry.Views
{
    public partial class LoginPage : ContentPage
    {
        public LoginPage()
        {
            InitializeComponent();

            // BindingContext = this;
            BindingContext = new LoginViewModel();
          //  ShowRequestAccess = true;
        }

        protected override async void OnAppearing()
        {


        }


        private void RequestAccessClicked()
        {

        }


    }
}

