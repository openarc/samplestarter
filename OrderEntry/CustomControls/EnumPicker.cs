﻿using System;
using Xamarin.Forms;
using System.Linq;
using System.ComponentModel.DataAnnotations;

namespace OrderEntry.CustomControls
{
	public class EnumPicker : Picker
	{
		#region Static Fields

		public static BindableProperty EnumSourceProperty =
			BindableProperty.Create<EnumPicker, Enum> (o => o.EnumSource, default (Enum), BindingMode.OneWay, null, OnEnumSourceChanged);

		public new static BindableProperty SelectedItemProperty =
			BindableProperty.Create<EnumPicker, object> (o => o.SelectedItem, default (object), BindingMode.TwoWay, null, OnSelectedItemChanged);

		#endregion

		#region Constructors and Destructors

		public EnumPicker ()
		{
			this.SelectedIndexChanged += this.OnSelectedIndexChanged;
		}

		#endregion

		#region Public Properties

		public Enum EnumSource {
			get {
				return (Enum)this.GetValue (EnumSourceProperty);
			}
			set {
				this.SetValue (EnumSourceProperty, value);
			}
		}

		public object SelectedItem {
			get {
				return this.GetValue (SelectedItemProperty);
			}
			set {
				this.SetValue (SelectedItemProperty, value);
			}
		}

		#endregion

		#region Methods

		private static void OnEnumSourceChanged (BindableObject bindable, Enum oldvalue, Enum newvalue)
		{
			var picker = bindable as EnumPicker;
			if (picker == null) {
				return;
			}
			if (newvalue == null) {
				return;
			}
			// Find the values not already contained in the Picker's list of items.
			var items = Enum.GetNames (newvalue.GetType ());
			var toBeAdded = items.Where (item => !picker.Items.Contains (item));
			foreach (var item in toBeAdded) {
				picker.Items.Add (item);
			}
		}

		private static void OnSelectedItemChanged (BindableObject bindable, object oldvalue, object newvalue)
		{
			var picker = bindable as EnumPicker;
			if (newvalue == null || (null != oldvalue && oldvalue.Equals (newvalue))) {
				return;
			}
			if (picker == null) {
				return;
			}
			var type = newvalue.GetType ();
			var values = Enum.GetValues (type);
			var items = (from object item in values select item).ToList ();
			picker.SelectedIndex = items.IndexOf (newvalue);
		}

		private void OnSelectedIndexChanged (object sender, EventArgs eventArgs)
		{
			var picker = sender as EnumPicker;
			if (null == picker || (this.SelectedIndex < 0 || this.SelectedIndex > this.Items.Count - 1)) {
				return;
			}

			var type = this.EnumSource.GetType ();
			var values = Enum.GetValues (type);
			var items = (from object item in values select item).ToList ();
			picker.SelectedItem = items [this.SelectedIndex];
		}



		#endregion
	}
}
