﻿using System;
using Xamarin.Forms;
using System.Linq;
using System.ComponentModel.DataAnnotations;

namespace OrderEntry.CustomControls
{
    public class EnumDescriptionPicker : Picker
    {
        #region Static Fields

        public static BindableProperty EnumSourceProperty =
            BindableProperty.Create<EnumDescriptionPicker, Enum>(o => o.EnumSource, default(Enum), BindingMode.OneWay, null, OnEnumSourceChanged);

        public static new BindableProperty SelectedItemProperty =
            BindableProperty.Create<EnumDescriptionPicker, object>(o => o.SelectedItem, default(object), BindingMode.TwoWay, null, OnSelectedItemChanged);

        #endregion

        #region Constructors and Destructors

        public EnumDescriptionPicker()
        {
            this.SelectedIndexChanged += this.OnSelectedIndexChanged;
        }

        #endregion

        #region Public Properties

        public Enum EnumSource
        {
            get
            {
                return (Enum)this.GetValue(EnumSourceProperty);
            }
            set
            {
                this.SetValue(EnumSourceProperty, value);
            }
        }

        public new object SelectedItem
        {
            get
            {
                return this.GetValue(SelectedItemProperty);
            }
            set
            {
                this.SetValue(SelectedItemProperty, value);
            }
        }

        #endregion

        #region Methods

        private static void OnEnumSourceChanged(BindableObject bindable, Enum oldValue, Enum newValue)
        {
            var picker = bindable as EnumDescriptionPicker;
            if (picker == null)
            {
                return;
            }
            if (newValue == null)
            {
                return;
            }
            // Find the values not already contained in the Picker's list of items.
            var items = EnumExtensions.GetDescriptions(newValue.GetType());
            var toBeAdded = items.Where(item => !picker.Items.Contains(item));
            foreach (var item in toBeAdded)
            {
                picker.Items.Add(item);
            }
        }

        private static void OnSelectedItemChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var picker = bindable as EnumDescriptionPicker;
            if (newValue == null || (null != oldValue && oldValue.Equals(newValue)))
            {
                return;
            }
            if (picker == null)
            {
                return;
            }

            picker.SelectedIndex = picker.Items.IndexOf(((Enum)newValue).GetDescription());
        }

        private void OnSelectedIndexChanged(object sender, EventArgs eventArgs)
        {
            var picker = sender as EnumDescriptionPicker;
            if (null == picker || (this.SelectedIndex < 0 || this.SelectedIndex > this.Items.Count - 1))
            {
                return;
            }

            var type = this.EnumSource.GetType();
            var values = EnumExtensions.GetDescriptions(type);
            var items = (from object item in values select item).ToList();
            picker.SelectedItem = EnumExtensions.GetEnumFromDescription(items[this.SelectedIndex].ToString(), type);
        }

        #endregion
    }
}
