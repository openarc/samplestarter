﻿using Xamarin.Forms;
namespace OrderEntry.CustomControls
{
    public class GradientColorStack : StackLayout
    {
        public Color StartColor
        {
            get;
            set;
        }
        public Color EndColor
        {
            get;
            set;
        }
    }
}

