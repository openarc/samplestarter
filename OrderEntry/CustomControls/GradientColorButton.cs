﻿using Xamarin.Forms;

namespace OrderEntry.CustomControls
{
    public class GradientColorButton : Button
    {
        public Color StartColor
        {
            get;
            set;
        }
        public Color EndColor
        {
            get;
            set;
        }
    }
}

