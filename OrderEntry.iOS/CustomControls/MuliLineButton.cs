﻿using OrderEntry.CustomControls;
using OrderEntry.iOS.CustomControls;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(MultiLineButton), typeof(MultiLineButtonIOS))]

namespace OrderEntry.iOS.CustomControls
{
    public class MultiLineButtonIOS : ButtonRenderer
    {
        public MultiLineButtonIOS()
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Button> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                Control.TitleLabel.LineBreakMode = UILineBreakMode.WordWrap;
                Control.TitleLabel.Lines = 0;
                Control.TitleLabel.TextAlignment = UITextAlignment.Center;
            }

        }
    }
}
