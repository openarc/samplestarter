﻿using CoreAnimation;
using CoreGraphics;
using OrderEntry.CustomControls;
using OrderEntry.iOS.CustomControls;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
[assembly: ExportRenderer(typeof(GradientColorButton), typeof(GradientColorButtonRenderer))]
namespace OrderEntry.iOS.CustomControls
{
    public class GradientColorButtonRenderer : VisualElementRenderer<Button>
    {
        public override void Draw(CGRect rect)
        {
            base.Draw(rect);
            GradientColorButton stack = (GradientColorButton)this.Element;
            CGColor startColor = stack.StartColor.ToCGColor();
            CGColor endColor = stack.EndColor.ToCGColor();
            #region for Vertical Gradient  
            //var gradientLayer = new CAGradientLayer();     
            #endregion
            #region for Horizontal Gradient  
            var gradientLayer = new CAGradientLayer()
            {
                StartPoint = new CGPoint(0, 0.5),
                EndPoint = new CGPoint(1, 0.5)
            };
            #endregion
            gradientLayer.Frame = rect;
          
            gradientLayer.Colors = new CGColor[] {
                startColor,
                endColor
            };
            gradientLayer.CornerRadius = rect.Height / 3;
          
            NativeView.Layer.InsertSublayerBelow (gradientLayer,NativeView.Layer);
            NativeView.Layer.SetNeedsLayout();
  
          

        }
    }
}
